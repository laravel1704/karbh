<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ToDoItem;

use Illuminate\Http\Request;

class ToDoController extends Controller
{
    public function index()
    {
        $todos = [
            new ToDoItem('Task 1', 'Description for Task 1'),
            new ToDoItem('Task 2', 'Description for Task 2'),
        ];

        return response()->json(['todos' => $todos]);
    }
}
