<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
// use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserController extends Controller
{
    public function signup(Request $request): JsonResponse
{
    $request->validate([
        'name' => 'required|string',
        'mobile_number' => 'required|digits:10|unique:users',
        'password' => 'required|string|alpha_num|min:8',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
    ]);

    if ($request->hasFile('image')) {
        $imagePath = $request->file('image')->store('images', 'public');
    } else {
        $imagePath = null;
    }

    $user = User::create([
        'name' => $request->input('name'),
        'mobile_number' => $request->input('mobile_number'),
        'password' => Hash::make($request->input('password')),
        'image' => $imagePath,
    ]);
    $token = $user->createToken('auth_token')->plainTextToken;
    return response()->json(['message' => 'User sign up done successfully','token' => $token],201);
}

public function signIn(Request $request)
{
    $request->validate([
        'mobile_number' => 'required|digits:10',
        'password' => 'required|string',
    ]);

    if (Auth::attempt(['mobile_number' => $request->input('mobile_number'), 'password' => $request->input('password')])) {
        $user = Auth::user();

        return response()->json(['message' => 'Sign in successful'],200);
    } else {
        return response()->json(['message' => 'Sign in failed. Invalid credentials'], 401);
    }
}

public function login(Request $request)
{
    $credentials = $request->only('mobile_number', 'password');

    try {
        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Invalid credentials'], 401);
        }
    } catch (JWTException $e) {
        return response()->json(['error' => 'Could not create token'], 500);
    }


    return response()->json(['message' => 'User logged in successfully', 'token' => $token]);
}

public function refresh()
{
    $token = JWTAuth::getToken();
    $newToken = JWTAuth::refresh($token);

    return response()->json(['message' => 'Token refreshed successfully', 'token' => $newToken]);
}

public function signout()
{
    try {
        JWTAuth::parseToken()->invalidate();

        return response()->json(['message' => 'User signed out successfully']);
    } catch (JWTException $e) {
        return response()->json(['error' => 'Failed to sign out'], 500);
    }
}
}
