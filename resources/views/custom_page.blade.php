@extends('layouts.app')

@section('content')

<!doctype html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:image" content>
<link rel="profile" href="https://gmpg.org/xfn/11">
<title>Tijarah &#8211; Multi-Vendor Digital Marketplace Theme</title>
<meta name="robots" content="max-image-preview:large" />
<link rel="dns-prefetch" href="//fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link rel="alternate" type="application/rss+xml" title="Tijarah &raquo; Feed" href="https://themebing.com/wp/tijarah/feed/" />
<link rel="alternate" type="application/rss+xml" title="Tijarah &raquo; Comments Feed" href="https://themebing.com/wp/tijarah/comments/feed/" />
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/themebing.com\/wp\/tijarah\/wp-includes\/js\/wp-emoji-release.min.js?ver=6.3.2"}};
/*! This file is auto-generated */
!function(i,n){var o,s,e;function c(e){try{var t={supportTests:e,timestamp:(new Date).valueOf()};sessionStorage.setItem(o,JSON.stringify(t))}catch(e){}}function p(e,t,n){e.clearRect(0,0,e.canvas.width,e.canvas.height),e.fillText(t,0,0);var t=new Uint32Array(e.getImageData(0,0,e.canvas.width,e.canvas.height).data),r=(e.clearRect(0,0,e.canvas.width,e.canvas.height),e.fillText(n,0,0),new Uint32Array(e.getImageData(0,0,e.canvas.width,e.canvas.height).data));return t.every(function(e,t){return e===r[t]})}function u(e,t,n){switch(t){case"flag":return n(e,"\ud83c\udff3\ufe0f\u200d\u26a7\ufe0f","\ud83c\udff3\ufe0f\u200b\u26a7\ufe0f")?!1:!n(e,"\ud83c\uddfa\ud83c\uddf3","\ud83c\uddfa\u200b\ud83c\uddf3")&&!n(e,"\ud83c\udff4\udb40\udc67\udb40\udc62\udb40\udc65\udb40\udc6e\udb40\udc67\udb40\udc7f","\ud83c\udff4\u200b\udb40\udc67\u200b\udb40\udc62\u200b\udb40\udc65\u200b\udb40\udc6e\u200b\udb40\udc67\u200b\udb40\udc7f");case"emoji":return!n(e,"\ud83e\udef1\ud83c\udffb\u200d\ud83e\udef2\ud83c\udfff","\ud83e\udef1\ud83c\udffb\u200b\ud83e\udef2\ud83c\udfff")}return!1}function f(e,t,n){var r="undefined"!=typeof WorkerGlobalScope&&self instanceof WorkerGlobalScope?new OffscreenCanvas(300,150):i.createElement("canvas"),a=r.getContext("2d",{willReadFrequently:!0}),o=(a.textBaseline="top",a.font="600 32px Arial",{});return e.forEach(function(e){o[e]=t(a,e,n)}),o}function t(e){var t=i.createElement("script");t.src=e,t.defer=!0,i.head.appendChild(t)}"undefined"!=typeof Promise&&(o="wpEmojiSettingsSupports",s=["flag","emoji"],n.supports={everything:!0,everythingExceptFlag:!0},e=new Promise(function(e){i.addEventListener("DOMContentLoaded",e,{once:!0})}),new Promise(function(t){var n=function(){try{var e=JSON.parse(sessionStorage.getItem(o));if("object"==typeof e&&"number"==typeof e.timestamp&&(new Date).valueOf()<e.timestamp+604800&&"object"==typeof e.supportTests)return e.supportTests}catch(e){}return null}();if(!n){if("undefined"!=typeof Worker&&"undefined"!=typeof OffscreenCanvas&&"undefined"!=typeof URL&&URL.createObjectURL&&"undefined"!=typeof Blob)try{var e="postMessage("+f.toString()+"("+[JSON.stringify(s),u.toString(),p.toString()].join(",")+"));",r=new Blob([e],{type:"text/javascript"}),a=new Worker(URL.createObjectURL(r),{name:"wpTestEmojiSupports"});return void(a.onmessage=function(e){c(n=e.data),a.terminate(),t(n)})}catch(e){}c(n=f(s,u,p))}t(n)}).then(function(e){for(var t in e)n.supports[t]=e[t],n.supports.everything=n.supports.everything&&n.supports[t],"flag"!==t&&(n.supports.everythingExceptFlag=n.supports.everythingExceptFlag&&n.supports[t]);n.supports.everythingExceptFlag=n.supports.everythingExceptFlag&&!n.supports.flag,n.DOMReady=!1,n.readyCallback=function(){n.DOMReady=!0}}).then(function(){return e}).then(function(){var e;n.supports.everything||(n.readyCallback(),(e=n.source||{}).concatemoji?t(e.concatemoji):e.wpemoji&&e.twemoji&&(t(e.twemoji),t(e.wpemoji)))}))}((window,document),window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 0.07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" id="wp-block-library-css" href="https://themebing.com/wp/tijarah/wp-includes/css/dist/block-library/style.min.css?ver=6.3.2" type="text/css" media="all" />
<style id="classic-theme-styles-inline-css" type="text/css">
/*! This file is auto-generated */
.wp-block-button__link{color:#fff;background-color:#32373c;border-radius:9999px;box-shadow:none;text-decoration:none;padding:calc(.667em + 2px) calc(1.333em + 2px);font-size:1.125em}.wp-block-file__button{background:#32373c;color:#fff;text-decoration:none}
</style>
<style id="global-styles-inline-css" type="text/css">
body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;--wp--preset--spacing--20: 0.44rem;--wp--preset--spacing--30: 0.67rem;--wp--preset--spacing--40: 1rem;--wp--preset--spacing--50: 1.5rem;--wp--preset--spacing--60: 2.25rem;--wp--preset--spacing--70: 3.38rem;--wp--preset--spacing--80: 5.06rem;--wp--preset--shadow--natural: 6px 6px 9px rgba(0, 0, 0, 0.2);--wp--preset--shadow--deep: 12px 12px 50px rgba(0, 0, 0, 0.4);--wp--preset--shadow--sharp: 6px 6px 0px rgba(0, 0, 0, 0.2);--wp--preset--shadow--outlined: 6px 6px 0px -3px rgba(255, 255, 255, 1), 6px 6px rgba(0, 0, 0, 1);--wp--preset--shadow--crisp: 6px 6px 0px rgba(0, 0, 0, 1);}:where(.is-layout-flex){gap: 0.5em;}:where(.is-layout-grid){gap: 0.5em;}body .is-layout-flow > .alignleft{float: left;margin-inline-start: 0;margin-inline-end: 2em;}body .is-layout-flow > .alignright{float: right;margin-inline-start: 2em;margin-inline-end: 0;}body .is-layout-flow > .aligncenter{margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > .alignleft{float: left;margin-inline-start: 0;margin-inline-end: 2em;}body .is-layout-constrained > .alignright{float: right;margin-inline-start: 2em;margin-inline-end: 0;}body .is-layout-constrained > .aligncenter{margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > :where(:not(.alignleft):not(.alignright):not(.alignfull)){max-width: var(--wp--style--global--content-size);margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > .alignwide{max-width: var(--wp--style--global--wide-size);}body .is-layout-flex{display: flex;}body .is-layout-flex{flex-wrap: wrap;align-items: center;}body .is-layout-flex > *{margin: 0;}body .is-layout-grid{display: grid;}body .is-layout-grid > *{margin: 0;}:where(.wp-block-columns.is-layout-flex){gap: 2em;}:where(.wp-block-columns.is-layout-grid){gap: 2em;}:where(.wp-block-post-template.is-layout-flex){gap: 1.25em;}:where(.wp-block-post-template.is-layout-grid){gap: 1.25em;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
.wp-block-navigation a:where(:not(.wp-element-button)){color: inherit;}
:where(.wp-block-post-template.is-layout-flex){gap: 1.25em;}:where(.wp-block-post-template.is-layout-grid){gap: 1.25em;}
:where(.wp-block-columns.is-layout-flex){gap: 2em;}:where(.wp-block-columns.is-layout-grid){gap: 2em;}
.wp-block-pullquote{font-size: 1.5em;line-height: 1.6;}
</style>
<link rel="stylesheet" id="contact-form-7-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.8.2" type="text/css" media="all" />
<link rel="stylesheet" id="redux-extendify-styles-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/redux-framework/redux-core/assets/css/extendify-utilities.css?ver=4.4.9" type="text/css" media="all" />
<link rel="stylesheet" id="e-animations-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.17.1" type="text/css" media="all" />
<link rel="stylesheet" id="tijarah-plugns-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/css/plugins.css?ver=6.3.2" type="text/css" media="all" />
<link rel="stylesheet" id="tijarah-plugn-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/css/plugin.css?ver=6.3.2" type="text/css" media="all" />
<style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel="stylesheet" id="tijarah-fonts-css" href="//fonts.googleapis.com/css?family=Rubik%3A300%2C400%2C500%2C700%2C900%26display%3Dswap&#038;ver=6.3.2" type="text/css" media="all" />
<link rel="stylesheet" id="tijarah-plugin-css" href="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/css/plugin.css?ver=6.3.2" type="text/css" media="all" />
<link rel="stylesheet" id="tijarah-style-css" href="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/style.css?ver=6.3.2" type="text/css" media="all" />
<style id="tijarah-style-inline-css" type="text/css">

	.preview-btn li a:hover,
	.call-to-action,
	#backtotop i,
	.comment-navigation .nav-links a,
	blockquote:before,
	.mean-container .mean-nav ul li a.mean-expand:hover,
	button, input[type="button"],
	.widget_price_filter .ui-slider .ui-slider-range,
	.widget_price_filter .ui-slider .ui-slider-handle,
	input[type="reset"],
	.off-canvas-menu .navigation li>a:hover,
	.off-canvas-menu .navigation .dropdown-btn:hover,
	.off-canvas-menu .navigation li .cart-contents,
	input[type="submit"],
	.tijarah-search-btn,
	.video-item .view-detail,
	.woocommerce-store-notice .woocommerce-store-notice__dismiss-link,
	.widget-product-details .widget-add-to-cart .variations .value .variation-radios [type="radio"]:checked + label:after,
	.widget-product-details .widget-add-to-cart .variations .value .variation-radios [type="radio"]:not(:checked) + label:after,
	.plyr__control--overlaid,
	.plyr--video .plyr__control.plyr__tab-focus,
	.plyr--video .plyr__control:hover,
	.plyr--video .plyr__control[aria-expanded=true],
	.product-social-share .float,
	.banner2 .banner-cat .cat-count,
	ul.banner-button li:first-child a,
	ul.banner-button li a:hover,
	.tijarah-pricing-table.recommended,
	.tijarah-pricing-table a:hover,
	.wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_parent > a, .wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_item > a, .wedocs-single-wrap .wedocs-sidebar ul.doc-nav-list > li.current_page_ancestor > a,
	.primary-menu ul li .children li.current-menu-item>a,
	.primary-menu ul li .sub-menu li.current-menu-item>a,
	.header-btn .sub-menu li.is-active a,
	.download-item-button a:hover,
    .recent-themes-widget,
    .newest-filter ul li.select-cat,
    .download-filter ul li.select-cat,
    .woocommerce .onsale,
    .download-item-overlay ul a:hover,
    .download-item-overlay ul a.active,
	input[type="button"],
	input[type="reset"],
	input[type="submit"],
	.checkout-button,
    .woocommerce-tabs ul.tabs li.active a:after,
    .tagcloud a:hover,
    .tijarah-btn,
    .dokan-btn,
    a.dokan-btn,
    .dokan-btn-theme,
    input[type="submit"].dokan-btn-theme,
	.tijarah-btn.bordered:hover,
    .testimonials-nav .slick-arrow:hover,
    .widget-woocommerce .single_add_to_cart_button,
    .post-navigation .nav-previous a ,
	.post-navigation .nav-next a,
	.blog-btn .btn:hover,
	.mean-container .mean-nav,
	.recent-theme-item .permalink,
	.banner-item-btn a,
	.meta-attributes li span a:hover,
	.theme-item-price span,
	.error-404 a,
	.mini-cart .widget_shopping_cart .woocommerce-mini-cart__buttons a,
	.download-item-image .onsale,
	.theme-item-btn a:hover,
	.theme-banner-btn a,
	.comment-list .comment-reply-link,
	.comment-form input[type=submit],
	.pagination .nav-links .page-numbers.current,
	.pagination .nav-links .page-numbers:hover,
	.breadcrumb-banner,
	.header-btn .children li a:hover,
	.primary-menu ul li .children li a:hover,
	.excerpt-date,
	.widget-title:after,
	.widget-title:before,
	.primary-menu ul li .sub-menu li a:hover,
	.header-btn .sub-menu li a:hover,
	.photo-product-item .button,
	.tags a:hover,
	.playerContainer .seekBar .outer .inner,
	.playerContainer .volumeControl .outer .inner,
	.excerpt-readmore a {
		background: #FF416C;
		background: -webkit-linear-gradient(to right, #FF416C, #FF4B2B);
		background: linear-gradient(to right, #FF416C, #FF4B2B);
	}

	.mini-cart .cart-contents:hover span,
	ul.banner-button li a,
	.order-again a,
	.testimonial-content>i,
	.testimonials-nav .slick-arrow,
	.tijarah-btn.bordered,
	.header-btn .my-account-btn,
	.primary-menu ul li.current-menu-item>a,
	.cat-links a,
	.plyr--full-ui input[type=range],
	.tijarah-team-social li a,
	.preview-btn li a,
	.related-post-title a:hover,
	.comment-author-link,
	.entry-meta ul li a:hover,
	.widget-product-details table td span a:hover,
	.woocommerce-message a,
	.woocommerce-info a,
	.footer-widget ul li a:hover,
	.woocommerce-noreviews a,
	.widget li a:hover,
	p.no-comments a,
	.woocommerce-notices-wrapper a,
	.woocommerce table td a,
	.blog-meta span,
	.blog-content h4:hover a,
	.tags-links a,
	.tags a,
	.woocommerce-account .woocommerce-MyAccount-navigation li.is-active a,
	.navbar-logo-text,
	.docs-single h4 a:hover,
	.docs-single ul li a:hover,
	.navbar .menu-item>.active,
	blockquote::before,
	.woocommerce-tabs ul.tabs li.active a,
	.woocommerce-tabs ul.tabs li a:hover,
	.primary-menu ul li>a:hover,
	.the_excerpt .entry-title a:hover{
		color: #FF416C;
	}


	.tijarah-btn.bordered,
	ul.banner-button li a,
	.testimonials-nav .slick-arrow,
	.my-account-btn,
	.widget-title,
	.preview-btn li a,
	.woocommerce-info,
    .download-item-overlay ul a:hover,
    .download-item-overlay ul a.active,
	.tijarah-pricing-table a,
	.woocommerce-MyAccount-navigation .is-active a,
	blockquote,
	.testimonials-nav .slick-arrow:hover,
	.loader,
	.uil-ripple-css div,
	.uil-ripple-css div:nth-of-type(1),
	.uil-ripple-css div:nth-of-type(2),
	.related-themes .single-related-theme:hover,
	.theme-author span,
	.tags a,
	.playerContainer,
	.sticky .the_excerpt_content {
		border-color: #FF416C!important;
	}


	.navbar-toggler-icon {
	  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='#FF416C' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
	}

	/*----------------------------------------
	IF SCREEN SIZE LESS THAN 769px WIDE
	------------------------------------------*/

	@media screen and (max-width: 768px) {
		.navbar .menu-item>.active {
	 		background: #FF416C;
		}
	}

</style>
<link rel="stylesheet" id="tijarah-woocommerce-style-css" href="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/css/woocommerce.css?ver=6.3.2" type="text/css" media="all" />
<style id="tijarah-woocommerce-style-inline-css" type="text/css">
@font-face {
			font-family: "star";
			src: url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.eot");
			src: url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.eot?#iefix") format("embedded-opentype"),
				url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.woff") format("woff"),
				url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.ttf") format("truetype"),
				url("https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/fonts/star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}
</style>
<link rel="stylesheet" id="elementor-icons-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.23.0" type="text/css" media="all" />
<link rel="stylesheet" id="elementor-frontend-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.17.1" type="text/css" media="all" />
<link rel="stylesheet" id="swiper-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/swiper/css/swiper.min.css?ver=5.3.6" type="text/css" media="all" />
<link rel="stylesheet" id="elementor-post-2251-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/post-2251.css?ver=1698274091" type="text/css" media="all" />
<link rel="stylesheet" id="font-awesome-5-all-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min.css?ver=3.17.1" type="text/css" media="all" />
<link rel="stylesheet" id="font-awesome-4-shim-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min.css?ver=3.17.1" type="text/css" media="all" />
<link rel="stylesheet" id="elementor-global-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/global.css?ver=1698274091" type="text/css" media="all" />
<link rel="stylesheet" id="elementor-post-70-css" href="https://themebing.com/wp/tijarah/wp-content/uploads/elementor/css/post-70.css?ver=1698274091" type="text/css" media="all" />
<link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Rubik:700&#038;display=swap&#038;ver=1672458661" /><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:700&#038;display=swap&#038;ver=1672458661" media="print" onload="this.media='all'"><noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:700&#038;display=swap&#038;ver=1672458661" /></noscript><link rel="stylesheet" id="google-fonts-1-css" href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=6.3.2" type="text/css" media="all" />
<link rel="stylesheet" id="elementor-icons-shared-0-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3" type="text/css" media="all" />
<link rel="stylesheet" id="elementor-icons-fa-solid-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3" type="text/css" media="all" />
<link rel="stylesheet" id="elementor-icons-fa-brands-css" href="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3" type="text/css" media="all" />
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin><script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/jquery/jquery.min.js?ver=3.7.0" id="jquery-core-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.4.1" id="jquery-migrate-js"></script>
<script type="text/javascript" id="tijarah_thumb_product_ajax_script-js-extra">
/* <![CDATA[ */
var tijarah_ajax_thumb_products_obj = {"tijarah_thumb_product_ajax_nonce":"c2e1aa7352","tijarah_thumb_product_ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/ajax-woo-thumb-products/ajax.js" id="tijarah_thumb_product_ajax_script-js"></script>
<script type="text/javascript" id="tijarah_product_ajax_script-js-extra">
/* <![CDATA[ */
var tijarah_ajax_products_obj = {"tijarah_product_ajax_nonce":"da44c24be1","tijarah_product_ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/ajax-woo-products/ajax.js" id="tijarah_product_ajax_script-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min.js?ver=3.17.1" id="font-awesome-4-shim-js"></script>
<link rel="https://api.w.org/" href="https://themebing.com/wp/tijarah/wp-json/" /><link rel="alternate" type="application/json" href="https://themebing.com/wp/tijarah/wp-json/wp/v2/pages/70" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://themebing.com/wp/tijarah/xmlrpc.php?rsd" />
<meta name="generator" content="WordPress 6.3.2" />
<meta name="generator" content="WooCommerce 8.2.1" />
<link rel="canonical" href="https://themebing.com/wp/tijarah/" />
<link rel="shortlink" href="https://themebing.com/wp/tijarah/" />
<link rel="alternate" type="application/json+oembed" href="https://themebing.com/wp/tijarah/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthemebing.com%2Fwp%2Ftijarah%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://themebing.com/wp/tijarah/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fthemebing.com%2Fwp%2Ftijarah%2F&#038;format=xml" />
<meta name="generator" content="Redux 4.4.9" />

<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<meta name="generator" content="Elementor 3.17.1; features: e_dom_optimization, e_optimized_assets_loading, additional_custom_breakpoints; settings: css_print_method-external, google_font-enabled, font_display-auto">
<link rel="icon" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail-32x32.png" sizes="32x32" />
<link rel="icon" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png" sizes="192x192" />
<link rel="apple-touch-icon" href="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png" />
<meta name="msapplication-TileImage" content="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png" />
<style id="tijarah_opt-dynamic-css" title="dynamic-css" class="redux-options-output">h1,h2,h3,h4,h5,h6{font-family:Rubik;font-weight:700;font-style:normal;color:#333;}body,p{font-family:Rubik;line-height:26px;font-weight:normal;font-style:normal;color:#808080;font-size:16px;}.breadcrumb-banner{background-image:url('https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/breadcrumb.jpg');}.breadcrumb-banner h1,.breadcrumbs ul li{color:#FFFFFF;}</style>
</head>
<body class="home page-template page-template-custom-homepage page-template-custom-homepage-php page page-id-70 wp-custom-logo theme-tijarah woocommerce-no-js woocommerce-active elementor-default elementor-kit-2251 elementor-page elementor-page-70">

<div id="preloader">
<div class="spinner">
<div class="uil-ripple-css" style="transform:scale(0.29);">
<div></div>
<div></div>
</div>
</div>
</div>
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<header class="site-header sticky-header">
<div class="container">
<div class="row align-items-center">
<div class="col-xl-2 col-md-3">
<div class="logo">
<a href="https://themebing.com/wp/tijarah/" class="custom-logo-link" rel="home" aria-current="page"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah" decoding="async" /></a> </div>
</div>
<div class="col-xl-8 col-md-9">
<div class="primary-menu d-none d-lg-inline-block float-right">
<nav class="desktop-menu">
<ul id="menu-primary" class="menu"><li id="menu-item-1971" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-70 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-1971"><a href="https://themebing.com/wp/tijarah/" aria-current="page">Home</a>
<ul class="sub-menu">
<li id="menu-item-282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-70 current_page_item menu-item-282"><a href="https://themebing.com/wp/tijarah/" aria-current="page">Home 1</a></li>
<li id="menu-item-1970" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1970"><a href="https://themebing.com/wp/tijarah/home-2/">Home 2</a></li>
<li id="menu-item-2385" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2385"><a href="https://themebing.com/wp/tijarah/videos/">Home 3 ( Video )</a></li>
<li id="menu-item-2386" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2386"><a href="https://themebing.com/wp/tijarah/images">Home 4 ( Stock Photography )</a></li>
<li id="menu-item-2387" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2387"><a href="https://themebing.com/wp/tijarah/audio/">Home 5 ( Audio )</a></li>
</ul>
</li>
<li id="menu-item-289" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-289"><a href="https://themebing.com/wp/tijarah/about/">About</a></li>
<li id="menu-item-285" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-285"><a href="https://themebing.com/wp/tijarah/shop/">Themes</a>
<ul class="sub-menu">
<li id="menu-item-2447" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2447"><a href="https://themebing.com/wp/tijarah/shop/">Shop Full Width</a></li>
<li id="menu-item-2445" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2445"><a href="https://themebing.com/wp/tijarah/shop/?shop_layout=left_sidebar">Shop Left Sidebar</a></li>
<li id="menu-item-2446" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2446"><a href="https://themebing.com/wp/tijarah/shop/?shop_layout=right_sidebar">Shop Right Sidebar</a></li>
</ul>
</li>
<li id="menu-item-2317" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2317"><a href="#">Pages</a>
<ul class="sub-menu">
<li id="menu-item-2357" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2357"><a href="https://themebing.com/wp/tijarah/services/">Services</a></li>
<li id="menu-item-2370" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2370"><a href="https://themebing.com/wp/tijarah/pricing-plan/">Pricing Plan</a></li>
<li id="menu-item-2381" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2381"><a href="https://themebing.com/wp/tijarah/team/">Team</a></li>
<li id="menu-item-2382" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2382"><a href="https://themebing.com/wp/tijarah/portfolio/">Portfolio</a></li>
<li id="menu-item-2364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2364"><a href="https://themebing.com/wp/tijarah/faq/">Faq</a></li>
<li id="menu-item-570" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-570"><a href="https://themebing.com/wp/tijarah/blog/">Blog</a></li>
<li id="menu-item-1958" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-1958"><a href="https://themebing.com/wp/tijarah/blog/the-best-advices-to-start-your-own-project/">Blog Details</a></li>
<li id="menu-item-2383" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2383"><a href="https://themebing.com/wp/tijarah/404">404</a></li>
</ul>
</li>
<li id="menu-item-290" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-290"><a href="https://themebing.com/wp/tijarah/contact/">Contact</a></li>
<li class="menu-cart">
<a class="cart-contents menu-item" href="https://themebing.com/wp/tijarah/cart/" title="View your shopping cart">
<span class="cart-contents-count"><i class="fa fa-shopping-cart"></i>
0 items </span>
</a>
<div class="mini-cart">
<div class="widget woocommerce widget_shopping_cart"><div class="widget_shopping_cart_content"></div></div> </div>
</li>
</ul> </nav>
</div>
</div>
<div class="col-xl-2 p-0 text-right">
<div class="header-btn d-none d-xl-block">
<a class="my-account-btn" href="https://themebing.com/wp/tijarah/my-account/">
<img src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/images/user.png" alt="Home">
My Account </a>
</div>
</div>
</div>
</div>
</header>

<div class="off-canvas-menu-bar">
<div class="container">
<div class="row">
<div class="col-8 my-auto">
<a href="https://themebing.com/wp/tijarah/" class="custom-logo-link" rel="home" aria-current="page"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah" decoding="async" /></a> </div>
<div class="col-2 my-auto">
<div class="header-btn float-right">
<a class="my-account-btn" href="https://themebing.com/wp/tijarah/my-account/">
<img src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/images/user.png" alt="Home">
</a>
</div>
</div>
<div class="col-2 my-auto">
<div class="mobile-nav-toggler"><span class="fas fa-bars"></span></div>
</div>
</div>
</div>
</div>

<div class="off-canvas-menu">
<div class="menu-backdrop"></div>
<i class="close-btn fa fa-close"></i>
<nav class="mobile-nav">
<div class="text-center pt-3 pb-3">
<a href="https://themebing.com/wp/tijarah/" class="custom-logo-link" rel="home" aria-current="page"><img width="260" height="56" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="custom-logo" alt="Tijarah" decoding="async" /></a> </div>
<ul class="navigation"></ul>
</nav>
</div>
<div data-elementor-type="wp-page" data-elementor-id="70" class="elementor elementor-70">
<section class="elementor-section elementor-top-section elementor-element elementor-element-cc2dcc7 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="cc2dcc7" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-dc95104" data-id="dc95104" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-a212b16 elementor-widget elementor-widget-banner" data-id="a212b16" data-element_type="widget" data-widget_type="banner.default">
<div class="elementor-widget-container">
<section class="banner ">
<div class="container">
<div class="row">
<div class="col-lg-7">
<div class="banner-content">
<h1>Best Themes and Plugins Marketplace</h1>
<p>Welcome to DigiMarket Multi vendor Marketplace Theme. Buy and Sell any kind of Digital Product you Wish. </p>
</div>
<div class="clearfix"></div>
<ul class="list-inline banner-button">
<li class="list-inline-item">
<a href="#">Shop Now</a>
</li>
<li class="list-inline-item">
<a href="#">Start Selling</a>
</li>
</ul>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-0936077 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0936077" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9d0aa12" data-id="9d0aa12" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-4006ada elementor-widget elementor-widget-partner" data-id="4006ada" data-element_type="widget" data-widget_type="partner.default">
<div class="elementor-widget-container">
<div class="container">
<div class="row align-items-center justify-content-center">
<div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
<div class="partner">
<img decoding="async" class="img-fluid" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/1.png" alt="Logo">
</div>
</div>
<div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
<div class="partner">
<img decoding="async" class="img-fluid" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/2.png" alt="Logo">
</div>
</div>
<div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
<div class="partner">
<img decoding="async" class="img-fluid" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/4.png" alt="Logo">
</div>
</div>
<div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
<div class="partner">
<img decoding="async" class="img-fluid" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/3.png" alt="Logo">
</div>
</div>
<div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
<div class="partner">
<img decoding="async" class="img-fluid" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/5.png" alt="Logo">
</div>
</div>
<div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
<div class="partner">
<img decoding="async" class="img-fluid" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/6.png" alt="Logo">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-b79f223 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="b79f223" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9791ff2" data-id="9791ff2" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<section class="elementor-section elementor-inner-section elementor-element elementor-element-c64fcbc elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="c64fcbc" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-335404c" data-id="335404c" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-6fe9493 elementor-widget elementor-widget-title" data-id="6fe9493" data-element_type="widget" data-widget_type="title.default">
<div class="elementor-widget-container">
<div class="section-title text-left">
<h1>Featured Items</h1>
<p>Nemo enim ipsam voluptatem quia voluptas aspernatur</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-81c383b elementor-widget elementor-widget-spacer" data-id="81c383b" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-17433ff" data-id="17433ff" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-7c0ff1d elementor-hidden-phone elementor-widget elementor-widget-button" data-id="7c0ff1d" data-element_type="widget" data-widget_type="button.default">
<div class="elementor-widget-container">
<div class="text-right">
<a class="tijarah-btn bordered elementor-inline-editing  " style="border-radius: 50px;" href="#">All Items</a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-inner-section elementor-element elementor-element-271ca69 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="271ca69" data-element_type="section">
<div class="elementor-container elementor-column-gap-no">
<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-03a006b" data-id="03a006b" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-9094459 elementor-widget elementor-widget-featured" data-id="9094459" data-element_type="widget" data-widget_type="featured.default">
<div class="elementor-widget-container">
<div class="container">
<div class="row justify-content-center">

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/">
<img fetchpriority="high" width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/">
<h5>Benchmark</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>59.00</bdi></span> &ndash; <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>199.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/" data-quantity="1" class="button product_type_variable add_to_cart_button" data-product_id="2177" data-product_sku aria-label="Select options for &ldquo;Benchmark&rdquo;" aria-describedby="This product has multiple variants. The options may be chosen on the product page" rel="nofollow">Select options</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/psd-templates/chapterone/">
<img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191116-5475-d3p1pb-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191116-5475-d3p1pb-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191116-5475-d3p1pb-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191116-5475-d3p1pb-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/psd-templates/chapterone/">
<h5>ChapterOne</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>74.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/psd-templates/chapterone/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2179" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2179" data-product_sku aria-label="Add &ldquo;ChapterOne&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/">
<img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/">
<h5>Avtorai</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>30.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2172" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2172" data-product_sku aria-label="Add &ldquo;Avtorai&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-f328f90 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="f328f90" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-ce0fdde" data-id="ce0fdde" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<section class="elementor-section elementor-inner-section elementor-element elementor-element-81211b7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="81211b7" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-740eda7" data-id="740eda7" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-372c9b8" data-id="372c9b8" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-b438b3e elementor-widget elementor-widget-title" data-id="b438b3e" data-element_type="widget" data-widget_type="title.default">
<div class="elementor-widget-container">
<div class="section-title text-center">
<h1>Why Choose DigiMarket</h1>
<p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-3a587f7 elementor-widget elementor-widget-spacer" data-id="3a587f7" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-783269e" data-id="783269e" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-inner-section elementor-element elementor-element-dbc5633 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="dbc5633" data-element_type="section">
<div class="elementor-container elementor-column-gap-extended">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2931051" data-id="2931051" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-9a7460d animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="9a7460d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-center" style="background: rgba(144, 19, 254, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fas fa-code"></i> <h5 style="color:#7D6E9B">We are Open Source</h5>
<p style="color:#7D6E9B">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-155a658" data-id="155a658" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-6e91566 animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="6e91566" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:400}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-center" style="background: rgba(43, 98, 201, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fas fa-lightbulb"></i> <h5 style="color:#707DAC">Problem Solvers</h5>
<p style="color:#707DAC">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6c18dce" data-id="6c18dce" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-77a1501 animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="77a1501" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:1000}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-center" style="background: rgba(26, 188, 156, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fab fa-superpowers"></i> <h5 style="color:#607E70">Regular Updates &amp; Bug fixes</h5>
<p style="color:#607E70">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-f5942e1 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="f5942e1" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-12ec057" data-id="12ec057" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<section class="elementor-section elementor-inner-section elementor-element elementor-element-24312a4 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="24312a4" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-98ba081" data-id="98ba081" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-a3abb1f" data-id="a3abb1f" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-3e96bd5 elementor-widget elementor-widget-title" data-id="3e96bd5" data-element_type="widget" data-widget_type="title.default">
<div class="elementor-widget-container">
<div class="section-title text-center">
<h1>Newly Added Items</h1>
<p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-09fd04a elementor-widget elementor-widget-spacer" data-id="09fd04a" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3b409b0" data-id="3b409b0" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-inner-section elementor-element elementor-element-be9eda1 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="be9eda1" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-314e47c" data-id="314e47c" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-ddaf748 elementor-widget elementor-widget-newest" data-id="ddaf748" data-element_type="widget" data-widget_type="newest.default">
<div class="elementor-widget-container">
<div class="newest-filter">
<ul class="list-inline">
<li class="select-cat list-inline-item thumb-product-filter" data-thumb-product-cat="wordpress,site-templates,cms-themes,ecommerce,psd-templates,joomla,">All Items</li>
<li class="list-inline-item thumb-product-filter" data-thumb-product-cat="wordpress">WordPress</li>
<li class="list-inline-item thumb-product-filter" data-thumb-product-cat="site-templates">Site Templates</li>
<li class="list-inline-item thumb-product-filter" data-thumb-product-cat="cms-themes">CMS Themes</li>
<li class="list-inline-item thumb-product-filter" data-thumb-product-cat="ecommerce">eCommerce</li>
<li class="list-inline-item thumb-product-filter" data-thumb-product-cat="psd-templates">PSD Templates</li>
<li class="list-inline-item thumb-product-filter" data-thumb-product-cat="joomla">Joomla</li>
</ul>
</div>
<div class="container loader-rel">
<div class="loader"></div>
<div class="newest_items row justify-content-center no-gutters">
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/wordpress/felipa/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-17.png" alt="Felipa - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-590x300.png" data-item-name="Felipa" data-item-cost data-item-category="WordPress" data-item-author="ThemeBing" data-title="Felipa">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/cms-themes/myagency/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-23.png" alt="MyAgency - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/2836341.jpg" data-item-name="MyAgency" data-item-cost data-item-category="CMS Themes" data-item-author="ThemeBing" data-title="MyAgency">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/appso/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-25.png" alt="AppSO - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-590x300.jpg" data-item-name="AppSO" data-item-cost="&#036;39" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="AppSO">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-4-1.png" alt="Benchmark - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-590x300.png" data-item-name="Benchmark" data-item-cost data-item-category="Joomla" data-item-author="ThemeBing" data-title="Benchmark">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/site-templates/munio/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-2.png" alt="Munio - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-590x300.png" data-item-name="Munio" data-item-cost="&#036;19" data-item-category="Site Templates" data-item-author="ThemeBing" data-title="Munio">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/psd-templates/chapterone/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-22.png" alt="ChapterOne - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191116-5475-d3p1pb-590x300.png" data-item-name="ChapterOne" data-item-cost="&#036;74" data-item-category="PSD Templates" data-item-author="ThemeBing" data-title="ChapterOne">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/cms-themes/instive/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-20.png" alt="Instive - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-590x300.png" data-item-name="Instive" data-item-cost="&#036;39" data-item-category="CMS Themes" data-item-author="ThemeBing" data-title="Instive">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/wordpress/jumpstart/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-19.png" alt="Jumpstart - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-590x300.jpg" data-item-name="Jumpstart" data-item-cost="&#036;16" data-item-category="WordPress" data-item-author="ThemeBing" data-title="Jumpstart">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-15.png" alt="Avtorai - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-590x300.png" data-item-name="Avtorai" data-item-cost="&#036;30" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="Avtorai">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/birdily/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-14.png" alt="Birdily - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-590x300.jpg" data-item-name="Birdily" data-item-cost="&#036;20" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="Birdily">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/cms-themes/fourmusic/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-16.png" alt="Fourmusic - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-590x300.png" data-item-name="Fourmusic" data-item-cost="&#036;59" data-item-category="CMS Themes" data-item-author="ThemeBing" data-title="Fourmusic">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/joomla/miion/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-13.png" alt="Miion - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-590x300.png" data-item-name="Miion" data-item-cost="&#036;49" data-item-category="Joomla" data-item-author="ThemeBing" data-title="Miion">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/site-templates/daeron/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-11.png" alt="Daeron - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191209-19043-10ih35o-590x300.png" data-item-name="Daeron" data-item-cost="&#036;59" data-item-category="Site Templates" data-item-author="ThemeBing" data-title="Daeron">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/jasy/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-10.png" alt="Jasy - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/2616398eaa72270b9f4ccca27ffb335c-590x300.png" data-item-name="Jasy" data-item-cost="Free" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="Jasy">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/psd-templates/rare-radio/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-12.png" alt="Rare Radio - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-590x300.png" data-item-name="Rare Radio" data-item-cost="&#036;59" data-item-category="PSD Templates" data-item-author="ThemeBing" data-title="Rare Radio">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/wordpress/livewell/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-9.png" alt="LiveWell - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191209-19043-10ih35o-590x300.png" data-item-name="LiveWell" data-item-cost="&#036;49" data-item-category="WordPress" data-item-author="ThemeBing" data-title="LiveWell">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/eaven/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-6.png" alt="Eaven - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-590x300.png" data-item-name="Eaven" data-item-cost="&#036;49" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="Eaven">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/cms-themes/scientia/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-8.png" alt="Scientia - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-590x300.png" data-item-name="Scientia" data-item-cost="&#036;89" data-item-category="CMS Themes" data-item-author="ThemeBing" data-title="Scientia">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/joomla/voicer/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-5.png" alt="Voicer - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-590x300.png" data-item-name="Voicer" data-item-cost="&#036;60" data-item-category="Joomla" data-item-author="ThemeBing" data-title="Voicer">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/psd-templates/growth/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-21.png" alt="Growth - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-590x300.jpg" data-item-name="Growth" data-item-cost="&#036;46" data-item-category="PSD Templates" data-item-author="ThemeBing" data-title="Growth">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/wordpress/amike/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/thumbnail.png" alt="Amike - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-590x300.png" data-item-name="Amike" data-item-cost="Free" data-item-category="WordPress" data-item-author="ThemeBing" data-title="Amike">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/biagiotti/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-1.png" alt="Biagiotti - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-590x300.png" data-item-name="Biagiotti" data-item-cost="&#036;59" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="Biagiotti">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/apza/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-14.png" alt="Apza - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-590x300.png" data-item-name="Apza" data-item-cost="&#036;49" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="Apza">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/site-templates/gutentim/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-25.png" alt="Gutentim - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-590x300.png" data-item-name="Gutentim" data-item-cost="&#036;39" data-item-category="Site Templates" data-item-author="ThemeBing" data-title="Gutentim">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/cms-themes/darpan/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-3.png" alt="Darpan - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191116-5475-d3p1pb-590x300.png" data-item-name="Darpan" data-item-cost="&#036;56" data-item-category="CMS Themes" data-item-author="ThemeBing" data-title="Darpan">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/cms-themes/scandi/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-13.png" alt="Scandi - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191116-5475-d3p1pb-590x300.png" data-item-name="Scandi" data-item-cost="&#036;67" data-item-category="CMS Themes" data-item-author="ThemeBing" data-title="Scandi">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/joomla/loveus/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-4-1.png" alt="Loveus - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-590x300.png" data-item-name="Loveus" data-item-cost="&#036;54" data-item-category="Joomla" data-item-author="ThemeBing" data-title="Loveus">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/wordpress/navian/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-23.png" alt="Navian - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-590x300.png" data-item-name="Navian" data-item-cost="&#036;49" data-item-category="WordPress" data-item-author="ThemeBing" data-title="Navian">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/verne/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-20.png" alt="Verne - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-590x300.jpg" data-item-name="Verne" data-item-cost="&#036;52" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="Verne">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/joomla/skudmart/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-18.png" alt="Skudmart - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191209-19043-10ih35o-590x300.png" data-item-name="Skudmart" data-item-cost="&#036;54" data-item-category="Joomla" data-item-author="ThemeBing" data-title="Skudmart">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/psd-templates/pawsitive/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-17.png" alt="Pawsitive - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-590x300.jpg" data-item-name="Pawsitive" data-item-cost="Free" data-item-category="PSD Templates" data-item-author="ThemeBing" data-title="Pawsitive">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/site-templates/evenz/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-16.png" alt="Evenz - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-590x300.png" data-item-name="Evenz" data-item-cost="&#036;45" data-item-category="Site Templates" data-item-author="ThemeBing" data-title="Evenz">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/wordpress/seolight/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-12.png" alt="Seolight - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/2616398eaa72270b9f4ccca27ffb335c-590x300.png" data-item-name="Seolight" data-item-cost="Free" data-item-category="WordPress" data-item-author="ThemeBing" data-title="Seolight">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/wordpress/cultivation/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-9.png" alt="Cultivation - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/2836341.jpg" data-item-name="Cultivation" data-item-cost="&#036;36" data-item-category="WordPress" data-item-author="ThemeBing" data-title="Cultivation">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/cms-themes/codrop/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-21.png" alt="Codrop - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-590x300.jpg" data-item-name="Codrop" data-item-cost="&#036;80" data-item-category="CMS Themes" data-item-author="ThemeBing" data-title="Codrop">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/ecommerce/tisara/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-2.png" alt="Tisara - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-590x300.jpg" data-item-name="Tisara" data-item-cost="&#036;75" data-item-category="eCommerce" data-item-author="ThemeBing" data-title="Tisara">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/joomla/acadevo/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-5.png" alt="Acadevo - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191209-19043-10ih35o-590x300.png" data-item-name="Acadevo" data-item-cost="&#036;60" data-item-category="Joomla" data-item-author="ThemeBing" data-title="Acadevo">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/psd-templates/amera/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-15.png" alt="Amera - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/2836341.jpg" data-item-name="Amera" data-item-cost="&#036;16" data-item-category="PSD Templates" data-item-author="ThemeBing" data-title="Amera">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/site-templates/arima/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-8.png" alt="Arima - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-32397-qhgwx7-590x300.png" data-item-name="Arima" data-item-cost="Free" data-item-category="Site Templates" data-item-author="ThemeBing" data-title="Arima">
</a>
</div>
<div class="col-auto">
<a class="site-preview" href="https://themebing.com/wp/tijarah/shop/wordpress/rion/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/02/th-10.png" alt="Rion - Photography WordPress Theme" data-preview-url="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-590x300.png" data-item-name="Rion" data-item-cost="&#036;39" data-item-category="WordPress" data-item-author="ThemeBing" data-title="Rion">
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-9f6daa8 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9f6daa8" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-fca60b5" data-id="fca60b5" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<section class="elementor-section elementor-inner-section elementor-element elementor-element-0d2c02d elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0d2c02d" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-293ad7b" data-id="293ad7b" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4ce5f2d" data-id="4ce5f2d" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-a2c89d8 elementor-widget elementor-widget-title" data-id="a2c89d8" data-element_type="widget" data-widget_type="title.default">
<div class="elementor-widget-container">
<div class="section-title text-center">
<h1>All Items</h1>
<p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-eea4a2b elementor-widget elementor-widget-spacer" data-id="eea4a2b" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-efe6ac0" data-id="efe6ac0" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-inner-section elementor-element elementor-element-0cc7f48 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0cc7f48" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-beab44b" data-id="beab44b" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-c864087 elementor-widget elementor-widget-download" data-id="c864087" data-element_type="widget" data-widget_type="download.default">
<div class="elementor-widget-container">
<div class="container">
<div class="download-filter">
<ul class="list-inline">
<li class="select-cat list-inline-item product-filter" data-product-cat="site-templates,cms-themes,ecommerce,joomla,">All Items</li>
<li class="list-inline-item product-filter" data-product-cat="site-templates">Site Templates</li>
<li class="list-inline-item product-filter" data-product-cat="cms-themes">CMS Themes</li>
<li class="list-inline-item product-filter" data-product-cat="ecommerce">eCommerce</li>
<li class="list-inline-item product-filter" data-product-cat="joomla">Joomla</li>
</ul>
</div>
<div class="loader"></div>
<div class="download_items row justify-content-center">
<div class="loader"></div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/myagency/">
<img loading="lazy" width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/2836341-750x430.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/myagency/">
<h5>MyAgency</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>59.00</bdi></span> &ndash; <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>199.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/myagency/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/myagency/" data-quantity="1" class="button product_type_variable add_to_cart_button" data-product_id="2181" data-product_sku aria-label="Select options for &ldquo;MyAgency&rdquo;" aria-describedby="This product has multiple variants. The options may be chosen on the product page" rel="nofollow">Select options</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/appso/">
<img loading="lazy" width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-750x430.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-750x430.jpg 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-500x286.jpg 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/learn-v2-350x200.jpg 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/appso/">
<h5>AppSO</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>39.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/appso/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2180" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2180" data-product_sku aria-label="Add &ldquo;AppSO&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/">
<img fetchpriority="high" width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/">
<h5>Benchmark</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>59.00</bdi></span> &ndash; <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>199.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/joomla/benchmark/" data-quantity="1" class="button product_type_variable add_to_cart_button" data-product_id="2177" data-product_sku aria-label="Select options for &ldquo;Benchmark&rdquo;" aria-describedby="This product has multiple variants. The options may be chosen on the product page" rel="nofollow">Select options</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/instive/">
<img loading="lazy" width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200109-18945-qpg2vi-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/instive/">
<h5>Instive</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>39.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/instive/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2178" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2178" data-product_sku aria-label="Add &ldquo;Instive&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/site-templates/munio/">
<img loading="lazy" width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191110-7219-13linqt-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
<span class="onsale">Sale!</span>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/site-templates/munio/">
<h5>Munio</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><del aria-hidden="true"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>49.00</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>19.00</bdi></span></ins></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/site-templates/munio/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2176" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2176" data-product_sku aria-label="Add &ldquo;Munio&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/fourmusic/">
<img loading="lazy" width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20200126-6689-ulcmi2-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/fourmusic/">
<h5>Fourmusic</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>59.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/cms-themes/fourmusic/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2173" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2173" data-product_sku aria-label="Add &ldquo;Fourmusic&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/">
<img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/">
<h5>Avtorai</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>30.00</bdi></span></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/avtorai/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2172" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2172" data-product_sku aria-label="Add &ldquo;Avtorai&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/birdily/">
<img loading="lazy" width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-750x430.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-750x430.jpg 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-300x171.jpg 300w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-500x286.jpg 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/3357768-350x200.jpg 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
<span class="onsale">Sale!</span>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/birdily/">
<h5>Birdily</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><del aria-hidden="true"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>30.00</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>20.00</bdi></span></ins></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/ecommerce/birdily/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2174" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2174" data-product_sku aria-label="Add &ldquo;Birdily&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="download-item">
<div class="download-item-image">
<a href="https://themebing.com/wp/tijarah/shop/joomla/miion/">
<img width="750" height="430" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-750x430.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt decoding="async" srcset="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-750x430.png 750w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-500x286.png 500w, https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/artboard_1-350x200.png 350w" sizes="(max-width: 750px) 100vw, 750px" /> </a>
<span class="onsale">Sale!</span>
</div>
<div class="download-item-content">
<a href="https://themebing.com/wp/tijarah/shop/joomla/miion/">
<h5>Miion</h5> </a>
<p>Photography WordPress Theme</p>
<ul class="list-inline mb-0">
<li class="list-inline-item">
<p class="price"><del aria-hidden="true"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>59.00</bdi></span></del> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">&#36;</span>49.00</bdi></span></ins></p>
</li>
<li class="list-inline-item float-right"></li>
</ul>
</div>
<div class="download-item-overlay">
<ul class="text-center mb-0 pl-0">
<li>
<a class="active" target="_blank" href="#"><i class="fa fa-eye"></i>Preview</a>
</li>
<li>
<a href="https://themebing.com/wp/tijarah/shop/joomla/miion/"><i class="fa fa-info-circle"></i>Details</a>
</li>
<li>
<a href="?add-to-cart=2171" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="2171" data-product_sku aria-label="Add &ldquo;Miion&rdquo; to your cart" aria-describedby rel="nofollow">Add to cart</a> </li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-element elementor-element-979ec8c elementor-widget elementor-widget-button" data-id="979ec8c" data-element_type="widget" data-widget_type="button.default">
<div class="elementor-widget-container">
<div class="text-center">
<a class="tijarah-btn  elementor-inline-editing shadow " style="border-radius: 50px;" href="https://themebing.com/wp/tijarah/shop/"><i class="fa fa-cart-arrow-down"></i>More Products</a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-19d8f9c elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="19d8f9c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-d9a0c29" data-id="d9a0c29" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-e9bb68d elementor-widget elementor-widget-counters" data-id="e9bb68d" data-element_type="widget" data-widget_type="counters.default">
<div class="elementor-widget-container">
<div class="counter-item text-center">
<div class="counter-icon">
<i aria-hidden="true" style="color:#FFFFFF" class="fab fa-envira"></i> </div>
<div class="elementor-counter-number-wrapper">
<div class="counter-content">
<h2 class="count elementor-counter-number" data-duration="2000" data-to-value="100" data-from-value="0" data-delimiter=" ">1032</h2>
<span>Total Items</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-1eaf49b" data-id="1eaf49b" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-b5de827 elementor-widget elementor-widget-counters" data-id="b5de827" data-element_type="widget" data-widget_type="counters.default">
<div class="elementor-widget-container">
<div class="counter-item text-center">
<div class="counter-icon">
<i aria-hidden="true" style="color:#FFFFFF" class="fab fa-app-store-ios"></i> </div>
<div class="elementor-counter-number-wrapper">
<div class="counter-content">
<h2 class="count elementor-counter-number" data-duration="2000" data-to-value="100" data-from-value="0" data-delimiter=" ">4892</h2>
<span>Total Sells</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-1d3ff61" data-id="1d3ff61" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-63b6b1a elementor-widget elementor-widget-counters" data-id="63b6b1a" data-element_type="widget" data-widget_type="counters.default">
<div class="elementor-widget-container">
<div class="counter-item text-center">
<div class="counter-icon">
<i aria-hidden="true" style="color:#FFFFFF" class="fas fa-user-friends"></i> </div>
<div class="elementor-counter-number-wrapper">
<div class="counter-content">
<h2 class="count elementor-counter-number" data-duration="2000" data-to-value="100" data-from-value="0" data-delimiter=" ">892</h2>
<span>Customers</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-9c672a9" data-id="9c672a9" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-a8e66ad elementor-widget elementor-widget-counters" data-id="a8e66ad" data-element_type="widget" data-widget_type="counters.default">
<div class="elementor-widget-container">
<div class="counter-item text-center">
<div class="counter-icon">
<i aria-hidden="true" style="color:#FFFFFF" class="fab fa-envira"></i> </div>
<div class="elementor-counter-number-wrapper">
<div class="counter-content">
<h2 class="count elementor-counter-number" data-duration="2000" data-to-value="100" data-from-value="0" data-delimiter=" ">6789</h2>
<span>Ratings</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-690780e elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="690780e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-11368fb" data-id="11368fb" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<section class="elementor-section elementor-inner-section elementor-element elementor-element-603e603 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="603e603" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3710f48" data-id="3710f48" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2cd958f" data-id="2cd958f" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-f87a6bc elementor-widget elementor-widget-title" data-id="f87a6bc" data-element_type="widget" data-widget_type="title.default">
<div class="elementor-widget-container">
<div class="section-title text-center">
<h1>DigiMarket Features</h1>
<p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-e107ee6 elementor-widget elementor-widget-spacer" data-id="e107ee6" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-8c78832" data-id="8c78832" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-inner-section elementor-element elementor-element-955841f elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="955841f" data-element_type="section">
<div class="elementor-container elementor-column-gap-extended">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-c9b1cda" data-id="c9b1cda" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-86ce87b animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="86ce87b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-left" style="background: rgba(144, 19, 254, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fas fa-code"></i> <h5 style="color:#7D6E9B">We are Open Source</h5>
<p style="color:#7D6E9B">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-aa4e39c animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="aa4e39c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-left" style="background: rgba(233, 79, 68, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fas fa-puzzle-piece"></i> <h5 style="color:#866E7D">Feature-Rich Free &amp; Pro Plugins</h5>
<p style="color:#866E7D">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-d6759bf" data-id="d6759bf" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-e3f9cd3 animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="e3f9cd3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:400}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-left" style="background: rgba(43, 98, 201, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fas fa-lightbulb"></i> <h5 style="color:#707DAC">Problem Solvers</h5>
<p style="color:#707DAC">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-0e55da7 animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="0e55da7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:1000}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-left" style="background: rgba(26, 188, 156, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fab fa-superpowers"></i> <h5 style="color:#607E70">Regular Updates &amp; Bug fixes</h5>
<p style="color:#607E70">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-83f5fad" data-id="83f5fad" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-f55702f animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="f55702f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:600}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-left" style="background: rgba(0, 130, 255, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fas fa-hands-helping"></i> <h5 style="color:#637595">Highly-Rated Support</h5>
<p style="color:#637595">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-21bbcca animated-slow elementor-invisible elementor-widget elementor-widget-InfoBox_item" data-id="21bbcca" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInLeft&quot;,&quot;_animation_delay&quot;:1200}" data-widget_type="InfoBox_item.default">
<div class="elementor-widget-container">
<div class="infobox-item style-2 text-left" style="background: rgba(245, 166, 35, 0.06)">
<i aria-hidden="true" style="color:#fff" class="fas fa-cog"></i> <h5 style="color:#897C66">Extensions to step-up your game</h5>
<p style="color:#897C66">Lorem ipsum dummy text in print and website industry are usually use in these section</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-1065f4b elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1065f4b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-fe47a62" data-id="fe47a62" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<section class="elementor-section elementor-inner-section elementor-element elementor-element-e230001 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="e230001" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6f3744e" data-id="6f3744e" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-9403ba3" data-id="9403ba3" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-d17f3a5 elementor-widget elementor-widget-title" data-id="d17f3a5" data-element_type="widget" data-widget_type="title.default">
<div class="elementor-widget-container">
<div class="section-title text-center">
<h1>Our Clients Feedback</h1>
<p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-373a161 elementor-widget elementor-widget-spacer" data-id="373a161" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
<div class="elementor-element elementor-element-f14b09b elementor-widget elementor-widget-spacer" data-id="f14b09b" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-b1b5bfe" data-id="b1b5bfe" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-inner-section elementor-element elementor-element-ff3f5b3 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="ff3f5b3" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-b02c8c0" data-id="b02c8c0" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-f2e10aa" data-id="f2e10aa" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-d74b378 elementor-widget elementor-widget-testimonials" data-id="d74b378" data-element_type="widget" data-widget_type="testimonials.default">
<div class="elementor-widget-container">
<div class="row align-items-center">
<div class="col-lg-4 col-md-4">
<div class="testimonials">
<div class="testimonial-img">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/t-1.jpg" alt="Emaley Mcculloch">
</div>
<div class="testimonial-img">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/t-2.jpg" alt="Nancy Pocker">
</div>
<div class="testimonial-img">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/t-3.jpg" alt="Nancoda Mao">
</div>
</div>
</div>
<div class="col-lg-7 col-md-8">
<div class="testimonials-nav">
<div class="testimonial-content">
<i class="fas fa-quote-left"></i>
<p>Awesome Product highly recomended Lorem ipsum dolor alamet, nsectetur mayalipol tempor eiusmod tempor recomended Lorem ipsum dolor alamet, nsec tetur mayalipol tempor eiusmod tempor incubto ectetur alasiqua enim ad nim veniam, quis nostrud ullam </p>
<div class="testi-bottom">
<div class="client-info">
<h4>Emaley Mcculloch</h4>
<span>Ui/Ux Designer &amp; Product Designer</span>
</div>
<ul class="list-inline">
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
</ul>
</div>
</div>
<div class="testimonial-content">
<i class="fas fa-quote-left"></i>
<p>Awesome Product highly recomended Lorem ipsum dolor alamet, nsectetur mayalipol tempor eiusmod tempor recomended Lorem ipsum dolor alamet, nsec tetur mayalipol tempor eiusmod tempor incubto ectetur alasiqua enim ad nim veniam, quis nostrud ullam </p>
<div class="testi-bottom">
<div class="client-info">
<h4>Nancy Pocker</h4>
<span>Ui/Ux Designer &amp; Product Designer</span>
</div>
<ul class="list-inline">
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
</ul>
</div>
</div>
<div class="testimonial-content">
<i class="fas fa-quote-left"></i>
<p>Awesome Product highly recomended Lorem ipsum dolor alamet, nsectetur mayalipol tempor eiusmod tempor recomended Lorem ipsum dolor alamet, nsec tetur mayalipol tempor eiusmod tempor incubto ectetur alasiqua enim ad nim veniam, quis nostrud ullam </p>
<div class="testi-bottom">
<div class="client-info">
<h4>Nancoda Mao</h4>
<span>Ui/Ux Designer &amp; Product Designer</span>
</div>
<ul class="list-inline">
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
<li class="list-inline-item"><i class="fa fa-star"></i></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-0a6ef89" data-id="0a6ef89" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-8816cc7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="8816cc7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c287882" data-id="c287882" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<section class="elementor-section elementor-inner-section elementor-element elementor-element-e563495 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="e563495" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-35b4fa2" data-id="35b4fa2" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-303383c" data-id="303383c" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-d2a131a elementor-widget elementor-widget-title" data-id="d2a131a" data-element_type="widget" data-widget_type="title.default">
<div class="elementor-widget-container">
<div class="section-title text-center">
<h1>Pricing Plan</h1>
<p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-fbee5a7 elementor-widget elementor-widget-spacer" data-id="fbee5a7" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-14a073e" data-id="14a073e" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-inner-section elementor-element elementor-element-947844a elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="947844a" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-502514e" data-id="502514e" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-7f63a9b elementor-widget elementor-widget-pricing_woocommerce" data-id="7f63a9b" data-element_type="widget" data-widget_type="pricing_woocommerce.default">
<div class="elementor-widget-container">
<div class="row justify-content-center">
<div class="col-lg-4 col-md-6">
<div class="tijarah-pricing-table">
<i aria-hidden="true" style="color:#fff; background-image: linear-gradient(180deg, #00e31d 0%, #00c0ce 180%);" class="fas fa-dice-d20"></i>
<h1 class="tijarah-price elementor-inline-editing">
<span>&#36;</span>
39 </h1>
<h6>Basic</h6>
<ul>
<li>Demo Content Install</li>
<li>Theme Updates</li>
<li>Support And Updates</li>
<li>Access All Themes</li>
<li>All Themes For Life</li>
</ul>
<a class="tijarah-buy-button" href="https://themebing.com/wp/tijarah/cart/?add-to-cart=2325">Purchase Now</a>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="tijarah-pricing-table">
<i aria-hidden="true" style="color:#fff; background-image: linear-gradient(180deg, #ff6167 0%, #ff0e27 180%);" class="fas fa-crown"></i>
<h1 class="tijarah-price elementor-inline-editing">
<span>&#36;</span>
59 </h1>
<h6>Standard</h6>
<ul>
<li>Demo Content Install</li>
<li>Theme Updates</li>
<li>Support And Updates</li>
<li>Access All Themes</li>
<li>All Themes For Life</li>
</ul>
<a class="tijarah-buy-button" href="https://themebing.com/wp/tijarah/cart/?add-to-cart=2326">Purchase Now</a>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="tijarah-pricing-table">
<i aria-hidden="true" style="color:#fff; background-image: linear-gradient(180deg, #00aeef 0%, #0f5ec9 180%);" class="fab fa-wordpress"></i>
<h1 class="tijarah-price elementor-inline-editing">
<span>&#36;</span>
120 </h1>
<h6>Premium</h6>
<ul>
<li>Demo Content Install</li>
<li>Premium Support</li>
<li>Support And Updates</li>
<li>Access All Themes</li>
<li>All Themes For Life</li>
</ul>
<a class="tijarah-buy-button" href="https://themebing.com/wp/tijarah/cart/?add-to-cart=2328">Purchase Now</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-9c5b553 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9c5b553" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-35e566b" data-id="35e566b" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<section class="elementor-section elementor-inner-section elementor-element elementor-element-765c11b elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="765c11b" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-8f1d7f9" data-id="8f1d7f9" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-31558b9" data-id="31558b9" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-89d1226 elementor-widget elementor-widget-title" data-id="89d1226" data-element_type="widget" data-widget_type="title.default">
<div class="elementor-widget-container">
<div class="section-title text-center">
<h1>Latest from Blog</h1>
<p>Anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined necessary.</p>
</div>
</div>
</div>
<div class="elementor-element elementor-element-8eb5e66 elementor-widget elementor-widget-spacer" data-id="8eb5e66" data-element_type="widget" data-widget_type="spacer.default">
<div class="elementor-widget-container">
<div class="elementor-spacer">
<div class="elementor-spacer-inner"></div>
</div>
</div>
</div>
</div>
</div>
<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-a58f854" data-id="a58f854" data-element_type="column">
<div class="elementor-widget-wrap">
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-inner-section elementor-element elementor-element-1e0f706 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1e0f706" data-element_type="section">
<div class="elementor-container elementor-column-gap-default">
<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d41038d" data-id="d41038d" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-1b5b5fa elementor-widget elementor-widget-blog" data-id="1b5b5fa" data-element_type="widget" data-widget_type="blog.default">
<div class="elementor-widget-container">
<div class="container">
<div class="row justify-content-center">

<div class="col-xl-4 col-md-6">
<div class="blog-item">
<div class="blog-thumb">
<a href="https://themebing.com/wp/tijarah/blog/20-best-free-html-marketplace-templates/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/leone-venter-pVt9j3iWtPM-unsplash-350x200.jpg" alt="20+ Best Free HTML Marketplace Templates">
</a>
</div>
<div class="blog-content">
<div class="blog-meta">
<a href="https://themebing.com/wp/tijarah/blog/author/admin/">ThemeBing</a>
<span> - April 3, 2018</span>
</div>
<h4><a href="https://themebing.com/wp/tijarah/blog/20-best-free-html-marketplace-templates/">20+ Best Free HTML Marketplace Templates</a></h4>
<p>Marketing is the and management of exchange relationships. Marketing ...</p>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="blog-item">
<div class="blog-thumb">
<a href="https://themebing.com/wp/tijarah/blog/best-free-responsive-wordpress-themes-in-2020/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/01/john-mark-arnold-ti4kGLkGgmU-unsplash-350x200.jpg" alt="Best Free Responsive WordPress Themes in 2020">
</a>
</div>
<div class="blog-content">
<div class="blog-meta">
<a href="https://themebing.com/wp/tijarah/blog/author/admin/">ThemeBing</a>
<span> - May 11, 2018</span>
</div>
<h4><a href="https://themebing.com/wp/tijarah/blog/best-free-responsive-wordpress-themes-in-2020/">Best Free Responsive WordPress Themes in...</a></h4>
<p>Marketing is the and management of exchange relationships. Marketing ...</p>
</div>
</div>
</div>

<div class="col-xl-4 col-md-6">
<div class="blog-item">
<div class="blog-thumb">
<a href="https://themebing.com/wp/tijarah/blog/top-web-design-trends-you-must-know-in-2020/">
<img decoding="async" src="https://themebing.com/wp/tijarah/wp-content/uploads/2019/10/image_processing20191219-10629-zhqr2q-350x200.png" alt="Top Web Design Trends You Must Know in 2020">
</a>
</div>
<div class="blog-content">
<div class="blog-meta">
<a href="https://themebing.com/wp/tijarah/blog/author/admin/">ThemeBing</a>
<span> - June 12, 2018</span>
</div>
<h4><a href="https://themebing.com/wp/tijarah/blog/top-web-design-trends-you-must-know-in-2020/">Top Web Design Trends You Must...</a></h4>
<p>Marketing is the and management of exchange relationships. Marketing ...</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</section>
<section class="elementor-section elementor-top-section elementor-element elementor-element-0c42353 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="0c42353" data-element_type="section">
<div class="elementor-container elementor-column-gap-no">
<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f0d6106" data-id="f0d6106" data-element_type="column">
<div class="elementor-widget-wrap elementor-element-populated">
<div class="elementor-element elementor-element-efa9302 elementor-widget elementor-widget-call_to_action" data-id="efa9302" data-element_type="widget" data-widget_type="call_to_action.default">
<div class="elementor-widget-container">
<section class="call-to-action">
<div class="container">
<div class="row justify-content-between">
<div class="col-md-7 text-left">
<h2>Join Us Today</h2>
<p class="mb-0">Over 75,000 designers and developers trust the DigiMarket.</p>
</div>
<div class="col-md-5 my-auto text-right">
<a class="tijarah-btn" href="#">Join Us Today</a>
</div>
</div>
</div>
</section>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<footer id="colophon" class="site-footer">
<div class="footer-widgets">
<div class="container">
<div class="row justify-content-xl-between">
<div class="col-lg-4">
<div id="custom_html-2" class="widget_text footer-widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="pr-5">
<img width="140" src="https://themebing.com/wp/tijarah/wp-content/uploads/2020/04/logo.png" class="img-fluid mb-4" alt="Tijarah">
<div class="footer-text mb-4">
<p>Popularised in the with the release of etras sheets containing passages and more rcently with desop publishing software like Maker including.</p>
</div>
<div class="footer-social">
<ul>
<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
<li><a href="#"><i class="fab fa-twitter"></i></a></li>
<li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
</ul>
</div>
</div></div></div> </div>
<div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
<div id="nav_menu-2" class="footer-widget widget_nav_menu"><h5 class="widget-title">Products</h5><div class="menu-help-support-container"><ul id="menu-help-support" class="menu"><li id="menu-item-2271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2271"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
<li id="menu-item-2273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2273"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
<li id="menu-item-2275" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2275"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
<li id="menu-item-2272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2272"><a href="https://themebing.com/wp/tijarah/contact/">Contact Us</a></li>
<li id="menu-item-2274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2274"><a href="https://themebing.com/wp/tijarah/shop/">Plugins</a></li>
</ul></div></div> </div>
<div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
<div id="nav_menu-3" class="footer-widget widget_nav_menu"><h5 class="widget-title">Resources</h5><div class="menu-our-company-container"><ul id="menu-our-company" class="menu"><li id="menu-item-2262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2262"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
<li id="menu-item-2268" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2268"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
<li id="menu-item-2270" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2270"><a href="https://themebing.com/wp/tijarah/shop/">Themes</a></li>
<li id="menu-item-2266" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2266"><a href="https://themebing.com/wp/tijarah/contact/">Contact Us</a></li>
<li id="menu-item-2267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2267"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
</ul></div></div> </div>
<div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
<div id="nav_menu-4" class="footer-widget widget_nav_menu"><h5 class="widget-title">Company</h5><div class="menu-help-support-container"><ul id="menu-help-support-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2271"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2273"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2275"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2272"><a href="https://themebing.com/wp/tijarah/contact/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2274"><a href="https://themebing.com/wp/tijarah/shop/">Plugins</a></li>
</ul></div></div> </div>
<div class="col-xl-2 col-lg-4 col-md-3 col-sm-6">
<div id="nav_menu-5" class="footer-widget widget_nav_menu"><h5 class="widget-title">Help and FAQs</h5><div class="menu-our-company-container"><ul id="menu-our-company-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2262"><a href="https://themebing.com/wp/tijarah/about/">About Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2268"><a href="https://themebing.com/wp/tijarah/my-account/">My account</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2270"><a href="https://themebing.com/wp/tijarah/shop/">Themes</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2266"><a href="https://themebing.com/wp/tijarah/contact/">Contact Us</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2267"><a href="https://themebing.com/wp/tijarah/checkout/">Checkout</a></li>
</ul></div></div> </div>
</div>
</div>
</div>
<div class="copyright-bar">
<div class="container">
<div class="row justify-content-center">
<div class="col-sm-7 text-center">
<p>
<p>Copyright © 2023 Tijarah All Rights Reserved.</p> </p>
</div>
</div>
</div>
</div>
</footer>

<div id="backtotop"><i class="fa fa-lg fa-arrow-up"></i></div>
<script type="text/javascript">
  function search_data_fetch(){
    jQuery.ajax({
        url: 'https://themebing.com/wp/tijarah/wp-admin/admin-ajax.php',
        type: 'post',
        data: { action: 'data_fetch', keyword: jQuery('#keyword').val() },
        success: function(data) {
          if (jQuery('#keyword').val().length !== 0) {
            jQuery('#datafetch').html( data );
          } else {
            jQuery('#datafetch').html( '' );
          }

        }
    });

    jQuery("#datafetch").show();
  }
  </script>
<script type="text/javascript">
		(function () {
			var c = document.body.className;
			c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
			document.body.className = c;
		})();
	</script>
<link rel="stylesheet" id="redux-custom-fonts-css" href="//themebing.com/wp/tijarah/wp-content/uploads/redux/custom-fonts/fonts.css?ver=1698855621" type="text/css" media="all" />
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/contact-form-7/includes/swv/js/index.js?ver=5.8.2" id="swv-js"></script>
<script type="text/javascript" id="contact-form-7-js-extra">
/* <![CDATA[ */
var wpcf7 = {"api":{"root":"https:\/\/themebing.com\/wp\/tijarah\/wp-json\/","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/contact-form-7/includes/js/index.js?ver=5.8.2" id="contact-form-7-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/js/plugins.js?ver=1.3.8" id="tijarah-plugins-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/tijarah-element/inc/../assets/js/plugin.js?ver=1.3.8" id="tijarah-plugin-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.7.0-wc.8.2.1" id="jquery-blockui-js"></script>
<script type="text/javascript" id="wc-add-to-cart-js-extra">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/themebing.com\/wp\/tijarah\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=8.2.1" id="wc-add-to-cart-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4-wc.8.2.1" id="js-cookie-js"></script>
<script type="text/javascript" id="woocommerce-js-extra">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=8.2.1" id="woocommerce-js"></script>
<script type="text/javascript" id="mailchimp-woocommerce-js-extra">
/* <![CDATA[ */
var mailchimp_public_data = {"site_url":"https:\/\/themebing.com\/wp\/tijarah","ajax_url":"https:\/\/themebing.com\/wp\/tijarah\/wp-admin\/admin-ajax.php","disable_carts":"","subscribers_only":"","language":"en","allowed_to_set_cookies":"1"};
/* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.min.js?ver=3.2.03" id="mailchimp-woocommerce-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/popper.min.js?ver=1.3.8" id="popper-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/bootstrap.min.js?ver=1.3.8" id="bootstrap-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/jquery.nice-select.min.js?ver=1.3.8" id="nice-select-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/plyr.min.js?ver=1.3.8" id="plyr-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/audio-player.js?ver=1.3.8" id="tijarah-audio-player-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/jquery.magnific-popup.min.js?ver=1.3.8" id="magnific-popup-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/skip-link-focus-fix.js?ver=1.3.8" id="tijarah-skip-link-focus-fix-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/themes/tijarah/assets/js/main.js?ver=1.3.8" id="tijarah-main-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce-gateway-paypal-express-checkout/assets/js/wc-gateway-ppec-order-review.js?ver=2.1.3" id="wc-gateway-ppec-order-review-js"></script>
<script type="text/javascript" id="wc-cart-fragments-js-extra">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp\/tijarah\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wp\/tijarah\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_ad5ba321e4aaaebc185a7ab437321a4b","fragment_name":"wc_fragments_ad5ba321e4aaaebc185a7ab437321a4b","request_timeout":"5000"};
/* ]]> */
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=8.2.1" id="wc-cart-fragments-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.17.1" id="elementor-webpack-runtime-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.17.1" id="elementor-frontend-modules-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2" id="elementor-waypoints-js"></script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-includes/js/jquery/ui/core.min.js?ver=1.13.2" id="jquery-ui-core-js"></script>
<script id="elementor-frontend-js-before" type="text/javascript">
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close","a11yCarouselWrapperAriaLabel":"Carousel | Horizontal scrolling: Arrow Left & Right","a11yCarouselPrevSlideMessage":"Previous slide","a11yCarouselNextSlideMessage":"Next slide","a11yCarouselFirstSlideMessage":"This is the first slide","a11yCarouselLastSlideMessage":"This is the last slide","a11yCarouselPaginationBulletMessage":"Go to slide"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile Portrait","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Landscape","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet Portrait","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Landscape","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.17.1","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"e_optimized_assets_loading":true,"additional_custom_breakpoints":true,"landing-pages":true},"urls":{"assets":"https:\/\/themebing.com\/wp\/tijarah\/wp-content\/plugins\/elementor\/assets\/"},"swiperClass":"swiper-container","settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","active_breakpoints":["viewport_mobile","viewport_tablet"],"lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":70,"title":"Tijarah%20%E2%80%93%20Multi-Vendor%20Digital%20Marketplace%20Theme","excerpt":"","featuredImage":false}};
</script>
<script type="text/javascript" src="https://themebing.com/wp/tijarah/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.17.1" id="elementor-frontend-js"></script>
</body>
</html>

@endsection
